// diapo
var i = 0;
var images = [];
var time = 3000;

//Image List
images[0] = "image1.jpg";
images[1] = "image2.jpg";
images[2] = "image3.jpg";
images[3] = "image4.jpg";

//Change Image
function changeImg() {
  document.slide.src = images[i];

  if (i < images.length - 1) {
    i++;
  } else {
    i = 0;
  }

  setTimeout("changeImg()", time);
}

// fonction manuelle diaporama
function slideImg(param) {
  var nowImg = slide.getAttribute("src");
  var index = images.indexOf(nowImg);

  if (param > 0) {
    index++;
    if (index > 3) {
      index = 0;
    }
  } else {
    index--;
    if (index < 0) {
      index = 3;
    }
  }
  var nextSlide = document.getElementById("slide");
  nextSlide.setAttribute("src", images[index]);
  console.log(nowImg);
  console.log(index);
  console.log(nextSlide);
}

window.onload = changeImg;

// horloge
var clock = document.getElementById("clock");
var hexColor = document.getElementById("hex-color");

function hexClock() {
  var time = new Date();
  var hours = time.getHours().toString();
  var minutes = time.getMinutes().toString();
  var seconds = time.getSeconds().toString();

  var hoursRandom = Math.ceil(Math.random() * 9);
  // console.log(hoursRandom);
  var minutesRandom = Math.ceil(Math.random() * 9);
  var secondsRandom = Math.ceil(Math.random() * 9);

  if (hours.length < 2) {
    hours = "0" + hours; // en cas de mono digit
  }

  if (minutes.length < 2) {
    minutes = "0" + minutes;
  }

  if (seconds.length < 2) {
    seconds = "0" + seconds;
  }

  var clockStr = hours + " : " + minutes + " : " + seconds; // affichage de l'horloge
  var hexColorStr = "#" + hoursRandom + minutesRandom + secondsRandom; // affichage du code hex par rapport a l'heure

  clock.textContent = clockStr; // applique la variable d'affichage

  if (time.getSeconds() % 2) {
    hexColor.textContent = hexColorStr;
    document.getElementById("clock").style.color = hexColorStr; // changement de la couleur de l'horloge avec le code hex chaque seconde
  }
}

hexClock();
setInterval(hexClock, 1000); // 1sec
